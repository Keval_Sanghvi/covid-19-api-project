(function ($, document, window) {

    $(document).ready(function () {

        // Cloning main navigation for mobile menu
        $(".mobile-navigation").append($(".main-navigation .menu").clone());

        // Mobile menu toggle 
        $(".menu-toggle").click(function () {
            $(".mobile-navigation").slideToggle();
        });
    });

    $(window).load(function () {

    });

})(jQuery, document, window);

document.addEventListener("DOMContentLoaded", addDate);

const defaultCountry = "India";
let country = "";
let trueFalse = false;

// get all three blocks for adding covid-data
const todayData = document.getElementById("todayData");
const yesterdayData = document.getElementById("yesterdayData");
const dayBeforeYesterdayData = document.getElementById("dayBeforeYesterdayData");

// get form element
const form = document.querySelector(".findData");

// get all checkboxes
const checkboxToday = document.getElementById("today");
const checkboxYesterday = document.getElementById("yesterday");
const checkboxDayBeforeYesterday = document.getElementById("dayBeforeYesterday");

checkboxDayBeforeYesterday.addEventListener("change", function () {
    if (trueFalse) {
        if (checkboxDayBeforeYesterday.checked) {
            get(country, false, false, true)
                .then(data => {
                    updateUI(data);
                    updateDayBeforeYesterday(data);
                })
                .catch(err => console.warn(err));
        } else {
            dayBeforeYesterdayData.innerHTML = "<p>Select the Date above to get Details!!!</p>";
        }
    }
});

checkboxYesterday.addEventListener("change", function () {
    if (trueFalse) {
        if (checkboxYesterday.checked) {
            get(country, false, true, false)
                .then(data => {
                    updateUI(data);
                    updateYesterday(data);
                })
                .catch(err => console.warn(err));
        } else {
            yesterdayData.innerHTML = "<p>Select the Date above to get Details!!!</p>";
        }
    }
});

checkboxToday.addEventListener("change", function () {
    if (trueFalse) {
        if (checkboxToday.checked) {
            get(country, true, false, false)
                .then(data => {
                    updateUI(data);
                    updateToday(data);
                })
                .catch(err => console.warn(err));
        } else {
            todayData.innerHTML = "<p>Select the Date above to get Details!!!</p>";
        }
    }
});

function addDate() {
    const todayDate = document.getElementById("todayDate");
    const yesterdayDate = document.getElementById("yesterdayDate");
    const dayBeforeYesterdayDate = document.getElementById("dayBeforeYesterdayDate");
    let d1 = new Date();
    let d2 = new Date();
    let d3 = new Date();
    d1.setDate(d1.getDate());
    d2.setDate(d1.getDate() - 1);
    d3.setDate(d1.getDate() - 2);
    d1 = d1.toString();
    d1 = d1.slice(4, 15);
    d2 = d2.toString();
    d2 = d2.slice(4, 15);
    d3 = d3.toString();
    d3 = d3.slice(4, 15);
    todayDate.childNodes[0].textContent = d1;
    yesterdayDate.childNodes[0].textContent = d2;
    dayBeforeYesterdayDate.childNodes[0].textContent = d3;
}

const updateUI = (data) => {

    let list = document.getElementsByClassName("data1");
    for (let i of list) {
        i.classList.remove("hidden");
    }
    const countryData = document.querySelector(".countryData");
    countryData.innerHTML = `<h1>${data.country}</h1><img src=${data.countryInfo.flag}>`;
}

function updateToday(data) {
    todayData.innerHTML = `<h3>Covid-19 Data</h3>
                            <p>Cases: ${data.todayCases}</p>
                            <p>TotalCases: ${data.cases}</p>
                            <p>Deaths: ${data.todayDeaths}</p>
                            <p>TotalDeaths: ${data.deaths}</p>
                            <p>Recovered: ${data.recovered}</p>
                            <p>TotalRecovered: ${data.todayRecovered}</p>
                            <p>Active: ${data.active}</p>`;
}

function updateYesterday(data) {
    yesterdayData.innerHTML = `<h3>Covid-19 Data</h3>
                            <p>Cases: ${data.todayCases}</p>
                            <p>TotalCases: ${data.cases}</p>
                            <p>Deaths: ${data.todayDeaths}</p>
                            <p>TotalDeaths: ${data.deaths}</p>
                            <p>Recovered: ${data.recovered}</p>
                            <p>TotalRecovered: ${data.todayRecovered}</p>
                            <p>Active: ${data.active}</p>`;
}

function updateDayBeforeYesterday(data) {
    dayBeforeYesterdayData.innerHTML = `<h3>Covid-19 Data</h3>
                            <p>Cases: ${data.todayCases}</p>
                            <p>TotalCases: ${data.cases}</p>
                            <p>Deaths: ${data.todayDeaths}</p>
                            <p>TotalDeaths: ${data.deaths}</p>
                            <p>Recovered: ${data.recovered}</p>
                            <p>TotalRecovered: ${data.todayRecovered}</p>
                            <p>Active: ${data.active}</p>`;
}

form.addEventListener("submit", e => {
    e.preventDefault();
    let countryName = form.country.value.trim();
    form.country.value = "";
    trueFalse = true;

    if (countryName == "") {
        countryName = defaultCountry;
    }
    country = countryName;

    get(countryName, true, false, false)
        .then(data => {
            updateUI(data);
        })
        .catch(err => console.warn(err));

    let yes1 = checkboxToday.checked;
    let yes2 = checkboxYesterday.checked;
    let yes3 = checkboxDayBeforeYesterday.checked;

    if (yes1 && yes2 && yes3) {
        get(countryName, true, false, false)
            .then(data => {
                updateUI(data);
                updateToday(data);
            })
            .catch(err => console.warn(err));
        get(countryName, false, true, false)
            .then(data => {
                updateUI(data);
                updateYesterday(data);
            })
            .catch(err => console.warn(err));
        get(countryName, false, false, true)
            .then(data => {
                updateUI(data);
                updateDayBeforeYesterday(data);
            })
            .catch(err => console.warn(err));
    }

    if (!yes1 && yes2 && yes3) {
        get(countryName, false, true, false)
            .then(data => {
                updateUI(data);
                updateYesterday(data);
            })
            .catch(err => console.warn(err));
        get(countryName, false, false, true)
            .then(data => {
                updateUI(data);
                updateDayBeforeYesterday(data);
            })
            .catch(err => console.warn(err));
    }

    if (yes1 && !yes2 && yes3) {
        get(countryName, true, false, false)
            .then(data => {
                updateUI(data);
                updateToday(data);
            })
            .catch(err => console.warn(err));
        get(countryName, false, false, true)
            .then(data => {
                updateUI(data);
                updateDayBeforeYesterday(data);
            })
            .catch(err => console.warn(err));
    }

    if (yes1 && yes2 && !yes3) {
        get(countryName, true, false, false)
            .then(data => {
                updateUI(data);
                updateToday(data);
            })
            .catch(err => console.warn(err));
        get(countryName, false, true, false)
            .then(data => {
                updateUI(data);
                updateYesterday(data);
            })
            .catch(err => console.warn(err));
    }

    if (yes1 && !yes2 && !yes3) {
        get(countryName, true, false, false)
            .then(data => {
                updateUI(data);
                updateToday(data);
            })
            .catch(err => console.warn(err));
    }

    if (!yes1 && yes2 && !yes3) {
        get(countryName, false, true, false)
            .then(data => {
                updateUI(data);
                updateYesterday(data);
            })
            .catch(err => console.warn(err));
    }

    if (!yes1 && !yes2 && yes3) {
        get(countryName, false, false, true)
            .then(data => {
                updateUI(data);
                updateDayBeforeYesterday(data);
            })
            .catch(err => console.warn(err));
    }

});
