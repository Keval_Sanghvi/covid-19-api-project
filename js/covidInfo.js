const get = async (country, yes1, yes2, yes3) => {
    const base = "https://disease.sh/v3/covid-19/countries/";
    let query = "";
    if (yes1) {
        query = `${country}?strict=true`;
    } else if (yes2) {
        query = `${country}?yesterday=${yes2}&strict=true`;
    } else if (yes3) {
        query = `${country}?twoDaysAgo=${yes3}&strict=true`;
    }
    const url = base + query;
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        return data;
    } else {
        await Promise.reject(new Error("Status Code: " + response.status));
    }
}
